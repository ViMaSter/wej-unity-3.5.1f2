#pragma strict
#pragma implicit
#pragma downcast

//settings
		var	XSpeed			: float		= 2000;
		var	YSpeed			: float		= 2000;
		var	MaxSpeed		: float		= 20;
		
		var	GJump			: float		= 200;
		var	AJump			: float		= 100;
		var AJumpFin		: float		= 100;
		
//menu priority
		var showGUI			: boolean	= true;
		var controlStuff	: boolean	= true;
		
//mobile settings
		var mobileJump		: boolean	= false;
		var xDeg			: float		= 0;
		var yDeg			: float		= 0;
		var zDeg			: float		= 0;
		
		var	audio1			: AudioSource; 
		var	audio2			: AudioSource; 

//internal and debug variables
		var jumped			: int		= 0;
		var iOSDeadzone		: float		= 0.1;
		var iOSTilt			: Vector3	= Vector3.zero;
private	var aJumpDeadz		: int		= 50;

// relations - Objects

// relations - Scripts

// relations - Get Objects on Startup
function setRelations () {
}

function Awake () {
	if (!networkView.isMine) {
		Destroy(gameObject.rigidbody);
		Destroy(this);
	}
	setRelations();
}

// Checks the touchscreen for a single press-input
function checkTouchJump () {
	if (Input.touchCount == 1) {
		for (var touch : Touch in Input.touches) {
			if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
				mobileJump = false;	
			}
			else if (touch.phase == TouchPhase.Began ) {
				mobileJump = true;
			}
		}
	}
}

// Resets the "normal" orientation to the one current present
function resetiOSOrientation () {
	iOSTilt = Input.acceleration;
	print ("Reset!");
}

function Update () {
	if (controlStuff) {
		checkControlInput();
	}
}

// Manages keyboard inputs
function checkControlInput () {
	// Jumping
	if (Input.GetButtonDown("Jump") || mobileJump) {
		mobileJump = false;
		if (rigidbody.velocity.y >= 0) {
			AJumpFin = AJump;
		}
		if (rigidbody.velocity.y < 0 && rigidbody.velocity.y >= -aJumpDeadz) {
			// Adds the falling velocity to the jumping power to increase the height
			AJumpFin = (AJump + -rigidbody.velocity.y * 50);
		}
		else if (rigidbody.velocity.y < -aJumpDeadz) {
			AJumpFin = AJump + aJumpDeadz * 50;
		}
		switch (jumped) {
			case 0:
				audio1.Play();
				rigidbody.AddForce(Vector3(0, GJump, 0));
				break;
			case 1:
				audio2.Play();
				rigidbody.AddForce(Vector3(0, AJumpFin, 0));
				break;
		}
		jumped++;
	}
	if (Input.touchCount == 3) {
		resetiOSOrientation();
	}
	
	// Movement
	checkTouchJump();
	if (rigidbody.velocity.x < MaxSpeed && rigidbody.velocity.z < MaxSpeed && rigidbody.velocity.magnitude < MaxSpeed) {
		fDir = transform.parent.Find("MainCamera").transform.forward.normalized;
		fDir.y = 0;
		rDir = transform.parent.Find("MainCamera").transform.right.normalized;
		rDir.y = 0;
		
		if (Input.acceleration.x - iOSTilt[1] > iOSDeadzone || Input.acceleration.y - iOSTilt[1] < (iOSDeadzone*-1)) {
			rigidbody.AddForce(((Input.acceleration.x - iOSTilt[1])*5*-1) * rDir * XSpeed * Time.deltaTime);		// iOS-Control
		}
		if (-Input.GetAxis("gHorizontal")) {
			rigidbody.AddForce((Input.GetAxis("gHorizontal")) * rDir * YSpeed * Time.deltaTime);					// Gamepad
		}
		if (-Input.GetAxis("Horizontal")) {
			rigidbody.AddForce((Input.GetAxis("Horizontal"))	* rDir * YSpeed * Time.deltaTime);					// Keyboard
		}
		
		if (Input.acceleration.x - iOSTilt[1] > iOSDeadzone || Input.acceleration.y - iOSTilt[1] < (iOSDeadzone*-1)) {
			rigidbody.AddForce(((Input.acceleration.y - iOSTilt[1])*5*-1) * fDir * XSpeed * Time.deltaTime);		// iOS-Control
		}
		if (-Input.GetAxis("gVertical")) {
			rigidbody.AddForce((-Input.GetAxis("gVertical")) * fDir * YSpeed * Time.deltaTime);						// Gamepad
		}
		if (-Input.GetAxis("Vertical")) {
			rigidbody.AddForce((Input.GetAxis("Vertical"))	* fDir * YSpeed * Time.deltaTime);						// Keyboard
		}
	}
}

function OnCollisionEnter(cObj : Collision) {
	// Resets the double jump while being on ground
	var fwd = Vector3(0, -1, 0);
	if (Physics.Raycast(transform.position, fwd, 1.5) && jumped != 0) {
		jumped = 0;
    }	
}
