#pragma strict
#pragma implicit
#pragma downcast

//settings
		var voteType		: int		= 0;
		var voteTarget		: String	= "";
		var voteTime		: float		= 0;
		var voteOpinion		: int		= 0;
		var voteResult		: int[];
		// 0	= Unset
		// 1	= False
		// 2	= True
		var playerArray		: NetworkPlayer[];
		
		var	langAtr			: Hashtable;
		var	mapAtr			: Hashtable;
		
private	var	voteState		: boolean	= false;
//internal and debug variables

// relations - Objects

// relations - Scripts

// relations - Get Objects on Startup
function setRelations () {
	langAtr				= new Hashtable();
	langAtr["free"]		= "Freeroaming";
	langAtr["time"]		= "Zeitrennen";
	langAtr["score"]	= "Punktejagd";
	
	mapAtr				= new Hashtable();
	mapAtr["1"]			= "Level 1";
	
	voteResult			= new int[2];
}

function Awake () {
	setRelations();
}

// Checks all input devices for a "Yes/No" if a vote is in progress
function Update	() {
	if (voteTime > 0) {
		voteTime -= Time.deltaTime;
		if(Input.GetAxisRaw("Vote") < 0)
			vote(1);
		if(Input.GetAxisRaw("Vote") > 0)
			vote(2);
	}
}

// Renders a voting window if the player isn't spectating
function OnGUI () {
	if ((GetComponent("Instantiate") as Instantiate).yourPlayer != null) {
		var inst : movement = (GetComponent("Instantiate") as Instantiate).yourPlayer.gameObject.Find("Player").GetComponent("movement") as movement;
		if (inst != null) {
			if (inst.showGUI) {
				if (voteTime > 0) {
					switch (voteType) {
						case 0:
							GUI.Label(Rect(10, Screen.height - 42.5, 400, 30), 'Spieler "' + langAtr[voteTarget] + '" kicken?' + " (" + voteTime.ToString("F2") + ")");
							break;
						case 1:
							GUI.Label(Rect(10, Screen.height - 42.5, 400, 30), 'Spieler "' + langAtr[voteTarget] + '" bannen?' + " (" + voteTime.ToString("F2") + ")");
							break;
						case 2:
							GUI.Label(Rect(10, Screen.height - 42.5, 400, 30), 'Spielmodus in "' + langAtr[voteTarget] + '" andern?' + " (" + voteTime.ToString("F2") + ")");
							break;
						case 3:
							GUI.Label(Rect(10, Screen.height - 42.5, 400, 30), 'Map "' + mapAtr[voteTarget] + '" laden?' + " (" + voteTime.	ToString("F2") + ")");
							break;
					}
					switch (voteOpinion) {
						case 0:
							GUI.Label(Rect(10, Screen.height - 22.5, 400, 30), 'F1: Zustimmen | F2: Ablehnen');
							break;
						case 1:
							GUI.Label(Rect(10, Screen.height - 22.5, 400, 30), 'Ablehnent!');
							break;
						case 2:
							GUI.Label(Rect(10, Screen.height - 22.5, 400, 30), 'Zugestimmt!');
							break;	
					}
				}
				else {
					if (voteState) {
						var height : int = 90;
						if (GUI.Button(Rect(0 + 10, Screen.height - height - 20 - 10, 100,20), "Schliessen"))
							voteState = false;
							var voteRect : Rect = Rect (0 + 10, Screen.height - height - 10, 390, height);
							voteRect = GUI.Window (0, voteRect, voteWindow, "Votes");
					}
					else {
						if (GUI.Button(Rect(0 + 10, Screen.height - 20 - 10, 100,20), "Votes"))
							voteState = true;
					}
				}
			}
		}
	}
}	

// Content of the vote options-window
function voteWindow() {
	var modes	: String[]	= new String[langAtr.Count];
	var maps	: String[]	= new String[mapAtr.Count];
	langAtr.Keys.CopyTo(	modes, 0);
	mapAtr.Keys.CopyTo(		maps, 0);
	var	h		: int		= 22.5;
	
	if (langAtr.Count) GUI.Label(Rect(10, h + 2.5, 90, 30), "Gamemodes:");
	for (i = 0; i < langAtr.Count; i++) {
		if (GUI.Button(Rect(100 + (95 * i), h, 90, 25), langAtr[modes[i]].ToString())) {
			callVote(2, modes[i]);
		}
	}
	h	= h + 35;
	
	if (mapAtr.Count) GUI.Label(Rect(10, h + 2.5, 90, 30), "Maps:");
	for (i = 0; i < mapAtr.Count; i++) {
		if (GUI.Button(Rect(100 + (95 * i), h, 90, 25), mapAtr[maps[i]].ToString())) {
			callVote(3, maps[i]);
		}
	}	
}

// Save and send choice to the server
function vote(opinion : int) {
	if (voteOpinion == 0) {
		voteOpinion = opinion;
		networkView.RPC("getVote", RPCMode.All, voteOpinion);	
	}
}
 
// Announces a vote to every client
function callVote(atr : int, atr_two : String) {
	var desiredTime : float	= 5;
	networkView.RPC("initVote", RPCMode.All, atr, atr_two, desiredTime);
}

// Starts a vote
@RPC
function initVote(atr : int, atr_two : String, choosenTime : float) {
	voteType			= atr;
	voteTarget			= atr_two;
	voteTime			= 5;
	yield WaitForSeconds(choosenTime);
	clearVote();
}

// Clears the voting space or changes the voted option
function clearVote () {
	// Yes:			voteResult[1]
	// No:			voteResult[0]
	var doIt : boolean = voteResult[1] > voteResult[0] ? true : false;
	if (Network.isServer) {
		if (doIt)
			doVote(voteType, voteTarget);
		else
			Debug.Log("Vote failed");// FAILED
	}
	voteType		= 0;
	voteTarget		= "";
	voteOpinion		= 0;
	voteResult		= new int[2];
}

// Receive a vote
@RPC
function getVote(atr : int) {
	voteResult[atr-1] = voteResult[atr-1] + 1;
}

// Actually "changes" the voted option 
function doVote(atr : int, target : String) {
	//	atr[0]:
	//		0 = kick
	//		1 = ban
	//		2 = gamemode-change
	//		3 = map-change
	lvlName	= Application.loadedLevelName.Split("_"[0])[0];
	type	= Application.loadedLevelName.Split("_"[0])[1];
	Debug.Log("DO: " + atr + "\n" + target);
	var	c : connectionGUI = GameObject.Find("network").GetComponent("connectionGUI") as connectionGUI;
	switch (atr) {
		case 0:
			//KICK-STUFF HERE
			break;
		case 1:
			//BAN-STUFF HERE
			break;
		case 2:
			//GameMode-Change
				c.globalLoadLevel(lvlName	+ "_" +	target);
			break;
		case 3:
			//Map-Change
				c.globalLoadLevel("lvl" + target	+ "_" +	type);
			break;	
	}
}