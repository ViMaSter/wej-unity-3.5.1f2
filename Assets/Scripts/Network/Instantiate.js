#pragma strict
#pragma implicit
#pragma downcast

//settings
		var PlayerPrefab	: Transform;
		var yourPlayer		: Transform;

//internal and debug variables

// relations - Objects

// relations - Scripts

// relations - Get Objects on Startup
function setRelations () {
	
}

function Awake () {
	setRelations();
}

function OnGUI () {
	if (yourPlayer) {
		var u : Username = yourPlayer.Find("Username").GetComponent("Username") as Username;
	}
}

function instantiatePlayer () {
	yourPlayer = Network.Instantiate(PlayerPrefab, transform.position, transform.rotation, 0);
	var g : gamemode = GetComponent("gamemode") as gamemode;
	g.setPlayerObj(yourPlayer);
}
function OnPlayerDisconnected (player : NetworkPlayer) {
	Network.RemoveRPCs(player, 0);
	Network.DestroyPlayerObjects(player);
}

function OnDisconnectedFromServer(info : NetworkDisconnection) {
    if (Network.isServer) {
        Debug.Log("Local server connection disconnected");
    }
    else {
        if (info == NetworkDisconnection.LostConnection)
            Debug.Log("Lost connection to the server");
        else
            Debug.Log("Successfully diconnected from the server");
    }
	Application.LoadLevel("menu");
}    