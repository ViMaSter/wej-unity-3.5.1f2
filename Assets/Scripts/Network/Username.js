#pragma strict
#pragma implicit
#pragma downcast

//settings
		var Username	: String	= "";
		var watching	: boolean	= false;
		
// relations - Objects
// relations - Scripts

// relations - Get Objects on Startup
function setRelations () {
}

function watch() {
	watching = true;
}

function Awake () {
	var tmp	: connectionGUI = GameObject.Find("network").GetComponent("connectionGUI") as connectionGUI;
	Username						=	tmp.Username;
	GetComponent(TextMesh).text		=	Username;
	networkView.RPC ("GetAuth", RPCMode.All);
	
	if (networkView.isMine) {
		var sc : scores = GameObject.Find("Spawn").GetComponent("scores") as scores;
		sc.myPlayer = Username;
		registerPlayer();
	}
}

function registerPlayer () {
	//Todo: Insert routine to Prevent Double-Naming here
	if (Network.isServer)
		GameObject.Find("Spawn").SendMessage("announcePlayer", Username);
	else
		GameObject.Find("Spawn").networkView.RPC("announcePlayer", RPCMode.Server, Username);
}

// "Hides" your own name
function Update () {
	if (networkView.isMine) {
		GetComponent(TextMesh).characterSize = 0;
	}
	else {
		transform.position = transform.parent.Find("Player").position + Vector3(0, 1.5, 0);
		if (watching)
			transform.eulerAngles.y = transform.parent.Find("MainCamera").eulerAngles.y;
		else
			transform.eulerAngles.y = transform.parent.Find("MainCamera").eulerAngles.y + 180;
	}
}

// Fetches usernames from every player
@RPC
function GetAuth (info : NetworkMessageInfo) {
	if (networkView.isMine) {
        var viewID : NetworkViewID = networkView.viewID;
	    networkView.RPC ("Auth", RPCMode.All, Username, viewID);
	}
}

// Applies names to the corresponding player objects
@RPC
function Auth (username : String, viewID : NetworkViewID, info : NetworkMessageInfo) {
    NetworkView.Find(viewID).transform.GetComponent(TextMesh).text = username;
}