#pragma strict
#pragma implicit
#pragma downcast

//version IMPORTANT FOR EXPORT AND VERSION INCOMPATIBILITY
private	var	versionID			: String	= "A 0.8";

//settings
		var menuActive			: boolean	= false;
		var scoreBoard			: boolean	= false;

		var Username			: String	= "Player";
		var remoteIP			: String	= "127.0.0.1";
		var remotePort			: int		= 8350;
		var remotePass			: String	= "";
		var useNAT				: boolean	= false;
		var activeLevel			: int		= 0;

//internal and debug variables
private	var	statMsg				: String	= "Noch keine Verbindung aufgebaut.";
private	var	browserMsg			: String	= "Keine Server verfügbar.";

private	var	showServer			: boolean	= false;
		var	showScoreboard		: boolean	= false;

private	var serverPings			: Ping[];
private var dPings				: String[];												//stores the final ping or "Ping..."

//server-creating variables
private	var	serverName			: String	= "WeJ-Default Server";
private	var	serverPassword		: String	= "";
private	var	serverLimit			: String	= "32";
private	var	serverPort			: String	= "8350";
private	var selectedMode		: int		= 0;
private	var selectedMap			: int		= 0;

//screen texts
private	var modeShort			: String[]	=	[	"free",
													"time",
													"score"
													];
private	var modeTitle			: String[]	=	[	"Freeroaming",
													"Zeitrennen",
													"Punktejagd"
													];
private	var mapTitle			: String[]	=	[	"Level 1"
													];
												
private	var gameModeTitle		: String[]	=	[	"Freeroaming",
													"Zeitrennen",
													"Punktejagd"
													];
private	var gameModeDesc		: String[]	=	[	"Im Freeroaming-Modus, gibt es weder Zeitdruck, noch einsammelbare Punkte. Hier kannst du Level erkunden oder einfach mit Freunden durch die Level hüpfen, um zum Beispiel eigene kleine Spielmodi auszuprobieren, bei denen du keine Zeitmessung oder Punkte brauchst.\nKeine Ziele, kein Gewinner.\n\n\nEin Spielbeitritt ist jederzeit möglich.",
													"Von A nach B, so schnell es geht. Zu Beginn einer Runde, wird - nachdem alle Spieler erfolgreich beigetreten sind - ein Countdown gezählt. Dann heißt es: So schnell wie möglich ans Ziel. Keine Items, keine Punkte. Das Spiel endet, nachdem alle Spieler das Ziel erreicht haben, oder 30 Sekunden, nachdem der erste Spieler das Ziel erreicht hat.\n\n\nEin Spielbeitritt ist nach jedem Spielbeginn möglich. Trittst du einem Spiel während einer Runde bei, bist du im Zuschauermodus, bis die Runde endet. Wechsel mit den Pfeiltasten links und rechts durch die einzelnen Spieler.",
													"Innerhalb eines festgelegten Zeitrahmens, geht es darum, so viele Punkte wie möglich einzusammeln. Das Spiel beginnt mit einem Countdown, wenn alle Spieler bereit sind. Nach Ablauf der Zeit, gewinnt der Spieler mit den meisten Punkten.\n\n\n\n\nEin Spielbeitritt ist nach jedem Spielbeginn möglich. Trittst du einem Spiel während einer Runde bei, bist du im Zuschauermodus, bis die Runde endet. Wechsel mit den Pfeiltasten links und rechts durch die einzelnen Spieler."
													];


//GUI-Rect
private	var	conRect				: Rect;
private	var	browserRect			: Rect;
private	var	gameRect			: Rect;
private	var	rulesRect			: Rect;
private	var	serverRect			: Rect;
private	var	scoreRect			: Rect;
		
		var lastLevelPrefix		: int		= 0;

		var	deactivated			: boolean	= false;
		
		var	centerAlign			: GUIStyle;
		var	centerCenterAlign	: GUIStyle;
		var	rightAlign			: GUIStyle;

// relations - Objects

// relations - Scripts

// relations - Get Objects on Startup
function setRelations () {
		Username		=	PlayerPrefs.GetString("username")	== ""	? "Player"		: PlayerPrefs.GetString("username");
		remoteIP		=	PlayerPrefs.GetString("remoteIP")	== ""	? "127.0.0.1"	: PlayerPrefs.GetString("remoteIP");
		remotePort		=	PlayerPrefs.GetInt("remotePort")	== 0	? 8350			: PlayerPrefs.GetInt("remotePort");
		conRect			=	Rect(0, 0, Screen.width - 655, 110);
		browserRect		=	Rect(0, 110, Screen.width - 400, Screen.height - 110);
		gameRect		=	Rect(Screen.width - 655, 0, 255, 110);
		serverRect		=	Rect(Screen.width - 400, Screen.height - 305, 400, 305);
		rulesRect		=	Rect(Screen.width - 400, 0, 400, Screen.height - 305);
		scoreRect		=	Rect(Screen.width / 2 - 350, Screen.height / 2 - 300, 700, 600);
}

function Awake () {
    DontDestroyOnLoad (gameObject);
    networkView.group = 1;
	setRelations();
	refreshServerlist();
}

// Method to queue "Loading..." or "Connecting..."
private	var	rotTime	: float;
private	var	j		: int		= 0;

function rotStat (s : String[]) {
	while (true) {
		if (rotTime == 0)
			rotTime = Time.time;
			
		if (Time.time - rotTime >= (j + 1) * 0.3)
			j++;
			
		if (j == s.Length) {
			rotTime = Time.time;
			j = 0;
		}
		if (Time.time - rotTime < (j + 1) * 0.3)
			statMsg = s[j];
			
		yield;
	}
}

function rotBrowse (s : String[]) {
	while (true) {
		if (rotTime == 0)
			rotTime = Time.time;
			
		if (Time.time - rotTime >= (j + 1) * 0.3)
			j++;
			
		if (j == s.Length) {
			rotTime = Time.time;
			j = 0;
		}
		if (Time.time - rotTime < (j + 1) * 0.3)
			browserMsg = s[j];
			
		yield;
	}
}

// Changes gamesmode in the menu
function changeMode (way : int) {
	if (selectedMode + way >= modeTitle.Length)		{selectedMode = 0;}
	else if (selectedMode + way < 0)				{selectedMode = modeTitle.Length - 1;}
	else											{selectedMode = selectedMode + way;}
}

// Changes gamesmode in the menu
function changeMap (way : int) {
	if (selectedMap + way >= mapTitle.Length)		{selectedMap = 0;}
	else if (selectedMap + way < 0)					{selectedMap = mapTitle.Length - 1;}
	else											{selectedMap = selectedMode+ way;}
}

// Used to connect with a server, after interface interaction (join from serverlist or connect-button)
// METHOD A: Using one ip (connect-button)

function establishConnection (ip : String, p : int) {
	if (Network.peerType != NetworkPeerType.Disconnected) {
		if (Network.isServer) MasterServer.UnregisterHost();
		Network.Disconnect(200);
	}
		
	if (Username != "") {
		PlayerPrefs.SetString("username",	Username);
		PlayerPrefs.SetString("remoteIP",	ip);
		PlayerPrefs.SetInt("remotePort",	p);
		StartCoroutine("rotStat", ["Verbinde", "Verbinde.", "Verbinde..", "Verbinde..."]);
		Network.Connect(ip, p, remotePass);
	}
	else {
		statMsg	= "Bitte gebe einen eigenen Username ein!";
		GUI.FocusControl("Username");
	}
}
//METHOD B: Using multiple ips (provided by the MasterServer/serverlist)
function establishConnection (ip : String[], p : int) {
	if(Username != "Player") {
		PlayerPrefs.SetString("username",	Username);
		PlayerPrefs.SetString("remoteIP",	ip[0]);
		PlayerPrefs.SetInt("remotePort",	p);
		StartCoroutine("rotStat", ["Verbinde", "Verbinde.", "Verbinde..", "Verbinde..."]);
		yield Network.Connect(ip, p, remotePass);
	}
	else {
		statMsg	= "Bitte gebe einen eigenen Username ein!";
		GUI.FocusControl("Username");
	}
}

function refreshServerlist () {
	showServer = false;
	StartCoroutine("rotBrowse", ["Aktuallisiere", "Aktuallisiere.", "Aktuallisiere..", "Aktuallisiere..."]);
	MasterServer.ClearHostList();
	MasterServer.RequestHostList("wej_mp_betaV02");
	rotTime	= 0;
	yield;
}

// After server list is updated, show it
function OnMasterServerEvent(msEvent: MasterServerEvent) {
	if (msEvent == MasterServerEvent.HostListReceived) {
		StopCoroutine("rotBrowse");
		showServer = true;
	}
}

function startServer() {
	statMsg = "Starte Server...";
	if(Username != "Player") {
		PlayerPrefs.SetString("username",	Username);
		try {
			var useNat = !Network.HavePublicAddress();
			Network.incomingPassword = serverPassword;
			Network.InitializeServer(System.Int32.Parse(serverLimit), System.Int32.Parse(serverPort), useNat);
			print("Open at: " + Network.player.ipAddress + ":" + serverPort.ToString() + "\n" + "Nat:" + useNat + " | " + "serverLimit: " + serverLimit);
			MasterServer.RegisterHost("wej_mp_betaV02", serverName, selectedMap + "|" + modeTitle[selectedMode] + "|" + versionID);
		}
		catch (e)
			statMsg = e.ToString(); // Notify our objects that the level and the network is ready
		globalLoadLevel("lvl" + (selectedMap + 1) + "_" + modeShort[selectedMode]);
	}
	else {
		statMsg	= "Bitte gebe einen eigenen Username ein!";
		GUI.FocusControl("Username");
	}
}

function Update () {
	if (Input.GetButtonDown("scoreboard"))		showScoreboard = true;
	if (Input.GetButtonUp("scoreboard"))		showScoreboard = false;
	if (serverPings != null) {
		if (serverPings.Length != 0) {
			for (i = 0; i < serverPings.length; i++) {
				if (serverPings[i].isDone) {
					dPings[i] = (serverPings[i].time + 0).ToString();
				}
				else
					dPings[i] = "Ping...";
			}
		}
	}
	if (GameObject.Find("Spawn") != null) {
		var inst : Instantiate = GameObject.Find("Spawn").GetComponent("Instantiate") as Instantiate;
		if (inst.yourPlayer != null) {
			var m : movement		= inst.yourPlayer.Find("Player").GetComponent("movement") as movement;
			if (Input.GetButtonDown("Menu")) {
				m.controlStuff = !m.controlStuff;
				menuActive = !menuActive;
			}
		}
	}
}

// direct-connect window
function connectionWindow (windowID : int) {
	GUI.SetNextControlName("Username");
	Username		=				GUI.TextField(new Rect (10, conRect.height - 30, 100, 20),	Username);
	GUI.SetNextControlName("IP");
	remoteIP		=				GUI.TextField(new Rect (125, conRect.height - 30, 100, 20),	remoteIP);
	GUI.SetNextControlName("Port");
	remotePort		=	parseInt(	GUI.TextField(new Rect (225, conRect.height - 30, 40, 20),	remotePort.ToString()));
	GUI.SetNextControlName("Passwort");
	remotePass		=				GUI.PasswordField(new Rect (280, conRect.height - 30, 100, 20),	remotePass, "*"[0], 16);
	
	GUI.Label(new Rect (10, 20, 605, 45), 	"Status: " + statMsg);
	GUI.Label(new Rect (10, conRect.height - 45, 100, 20), 	"Username",				centerAlign);
	GUI.Label(new Rect (125, conRect.height - 45, 100, 20),	"IP",					centerAlign);
	GUI.Label(new Rect (225, conRect.height - 45, 40, 20),	"Port",					centerAlign);
	GUI.Label(new Rect (280, conRect.height - 45, 100, 20),	"Server-Passwort",		centerAlign);
	if (GUI.Button (new Rect(conRect.width	- 135, conRect.height - 42.5, 125, 30), Network.peerType == NetworkPeerType.Disconnected ? "Connect" : "Disconnect")) {
		if (NetworkPeerType.Disconnected)
			establishConnection(remoteIP, remotePort);
		else {
			// Disconnect from the server
			if (Network.isServer) MasterServer.UnregisterHost();
			Network.Disconnect(200);
			refreshServerlist();
		}
	}
	
	/*if (GUI.Button (new Rect(conRect.width	- 135, conRect.height - 85, 125, 30), "Start Server")) {
			startServer();
	}*/
}

// game-info window
function gameWindow (windowID : int) {
	if (GUI.Button(Rect(gameRect.width / 2 - 100, gameRect.height / 2 - 25, 200, 50), "Beenden")) Application.Quit();
}
// server browser
function serverBrowser (windowID : int) {
	var hostData : HostData;
	if (GUI.Button (new Rect(browserRect.width - 100, 0, 100, 20), "Refresh"))
		refreshServerlist();
		
	var serverList : Array = MasterServer.PollHostList();
	if (serverList.length == 0)
		GUI.Label(new Rect(browserRect.width / 2 - 300, browserRect.height / 2 - 25, 600, 50), showServer ? "Keine Server verfügbar." : browserMsg, centerAlign);
	else {
			serverPings	=	new Ping[serverList.length];
			dPings		= new String[serverList.length];
			
			GUI.Label (Rect(0, 10, Screen.width - 1030, 30), "Name", centerCenterAlign);
			GUI.Label (Rect(Screen.width - 1030, 10, 100, 30), "Map", centerCenterAlign);
			GUI.Label (Rect(Screen.width - 930, 10, 100, 30), "Mode", centerCenterAlign);
			GUI.Label (Rect(Screen.width - 830, 10, 100, 30), "Spieler", centerCenterAlign);
			GUI.Label (Rect(Screen.width - 730, 10, 75, 30), "Passwort", centerCenterAlign);
			GUI.Label (Rect(Screen.width - 655, 10, 75, 30), "Version", centerCenterAlign);
			GUI.Label (Rect(Screen.width - 580, 10, 75, 30), "Ping", centerCenterAlign);
			
			for (i = 0; i < serverList.length; i++) {
				hostData		= serverList[i];
				dIp				= hostData.ip.GetType() == String ? hostData.ip.ToString() : hostData.ip[0].ToString();
				serverPings[i]	= Ping(dIp);
				dPort			= hostData.port;
				dName			= hostData.gameName;
				dMap			= hostData.comment.Split("|"[0])[0];
				dMode			= hostData.comment.Split("|"[0])[1];
				dVers			= hostData.comment.Split("|"[0])[2];
				dPlyr			= hostData.connectedPlayers.ToString();
				dLimit			= hostData.playerLimit.ToString();
				dPass			= hostData.passwordProtected ? "textures/server/lock_on" : "textures/server/lock_off";
				GUI.Label (Rect(0, 35 + 25 * i, Screen.width - 1030, 20), dName, centerCenterAlign);
				GUI.Label (Rect(Screen.width - 1030, 35 + 25 * i, 100, 20), dMap, centerCenterAlign);
				GUI.Label (Rect(Screen.width - 930, 35 + 25 * i, 100, 20), dMode, centerCenterAlign);
				GUI.Label (Rect(Screen.width - 830, 35 + 25 * i, 100, 20), (dPlyr) + "/" + (System.Int32.Parse(dLimit) - 1).ToString(), centerCenterAlign);
				GUI.DrawTexture (Rect(Screen.width - 703.5, 37 + 25 * i, 20, 20), Resources.Load(dPass) as Texture);
				GUI.Label (Rect(Screen.width - 655, 35 + 25 * i, 75, 20), dVers, centerCenterAlign);
				GUI.Label (Rect(Screen.width - 590, 35 + 25 * i, 75, 20), dPings[i], centerCenterAlign);
				
				if (dVers == versionID)
					if (GUI.Button (Rect(Screen.width - 500, 35 + 25 * i, 100, 20), "Beitreten")) {
						establishConnection(dIp, dPort);	
					}
			}
	}
}

// server creation window
function serverCreation (windowID : int) {
	if (Network.peerType == NetworkPeerType.Disconnected) {
		//serverRect
		if (GUI.Button (new Rect(serverRect.width - 110, serverRect.height - 30, 100, 20), "Start Server")) startServer();
		
		GUI.Label (new Rect(20, 30, 100, 30), "Servername:");
		serverName			=		GUI.TextField(new Rect		(serverRect.width - 200, 30, 170, 20),	serverName);
		
		GUI.Label (new Rect(20, 60, 100, 30), "Passwort:");
		serverPassword		=		GUI.PasswordField(new Rect	(serverRect.width - 200, 60, 170, 20),	serverPassword, "*"[0], 16);
		
		GUI.Label (new Rect(20, 90, 100, 30), "Map:");
		GUI.Label (new Rect(serverRect.width - 180, 90, 130, 20), mapTitle[selectedMap], centerCenterAlign);	
		if (GUI.Button (new Rect(serverRect.width - 200, 90, 20, 20), "<"))		changeMap(-1);	
		if (GUI.Button (new Rect(serverRect.width - 50, 90, 20, 20), ">"))		changeMap(1);
		
		GUI.Label (new Rect(20, 120, 100, 30), "Gamemode:");
		GUI.Label (new Rect(serverRect.width - 180, 120, 130, 20), modeTitle[selectedMode], centerCenterAlign);	
		if (GUI.Button (new Rect(serverRect.width - 200, 120, 20, 20), "<"))		changeMode(-1);
		if (GUI.Button (new Rect(serverRect.width - 50, 120, 20, 20), ">"))		changeMode(1);
		
		GUI.Label (new Rect(20, 150, 100, 30), "Spielerlimit:");
		serverLimit			=		GUI.TextField(new Rect (serverRect.width - 200, 150, 170, 20),	serverLimit);
			
		GUI.Label (new Rect(20, 180, 100, 30), "Port:");
		serverPort			=		GUI.TextField(new Rect (serverRect.width - 200, 180, 170, 20),	serverPort);
	}
	else {
		GUI.Label (new Rect(20, 30, 100, 30), "Map:");
		GUI.Label (new Rect(serverRect.width - 180, 30, 130, 20), mapTitle[selectedMap], centerCenterAlign);	
		
		GUI.Label (new Rect(20, 60, 100, 30), "Gamemode:");
		GUI.Label (new Rect(serverRect.width - 180, 60, 130, 20), modeTitle[selectedMode], centerCenterAlign);	
		
		GUI.Label (new Rect(20, 90, 100, 30), "Spielerlimit:");
		GUI.Label (new Rect (serverRect.width - 200, 90, 170, 20),	serverLimit, centerCenterAlign);
			
		GUI.Label (new Rect(20, 120, 100, 30), "Port:");
		GUI.Label (new Rect (serverRect.width - 200, 120, 170, 20),	serverPort, centerCenterAlign);
			
		ipaddress = Network.player.ipAddress;
		port = Network.player.port.ToString();
		
		GUI.Label (new Rect(20, 150, 100, 30), Network.isServer ? "Eigene IP:" : "Verbunden mit:");
		GUI.Label (new Rect(serverRect.width - 200, 150, 170, 20),	Network.isServer ? (ipaddress + ":" + port) : (remoteIP + ":" + remotePort), centerCenterAlign);
	
		if (GUI.Button (new Rect(serverRect.width - 110, serverRect.height - 40,100,30),"Disconnect")) {
			// Disconnect from the server
			if (Network.isServer) MasterServer.UnregisterHost();
			Network.Disconnect(200);
			refreshServerlist();
		}
	}
}

// rules-window
function rules (windowID : int) {
	var h : int		= 20;
	GUI.Label	(Rect(20, h, 360, 35),	"Steuerung:");																h = h + 20;
	GUI.Label	(Rect(20, h, 360, 35),	"W	/ Hoch		= Nach vorne rollen");										h = h + 20;
	GUI.Label	(Rect(20, h, 360, 35),	"S	/ Runter		= Nach hinten rollen");									h = h + 20;
	GUI.Label	(Rect(20, h, 360, 35),	"A	/ Links		= Nach links rollen");										h = h + 20;
	GUI.Label	(Rect(20, h, 360, 35),	"D	/ Rechts	= Nach rechts rollen");										h = h + 20;
	GUI.Label	(Rect(20, h, 360, 45),	"Linke Maustaste halten und die Maus bewegen, um die Kamera zu drehen.");	h = h + 45;
	GUI.Label	(Rect(20, h, 360, 35),	"Spielmodi:");																h = h + 20;
	GUI.Label	(Rect(20, h, 360, 35),	gameModeTitle[selectedMode]);												h = h + 20;
	GUI.Label	(Rect(20, h, 360, 230),	gameModeDesc[selectedMode]);
	
	h = h + 240;
}

//GUI content for the rules-container
function score (windowID : int) {
	// TODO: Add scoreboard GUI info here
}

function OnGUI () {
	if (Network.peerType == NetworkPeerType.Disconnected || (menuActive && Network.peerType != NetworkPeerType.Disconnected)) {
		conRect			= GUI.Window ("Spiel beitreten".GetHashCode(), conRect, connectionWindow, "Spiel beitreten");
		browserRect		= GUI.Window ("Serverbrowser".GetHashCode(), browserRect, serverBrowser, "Serverbrowser");
		gameRect		= GUI.Window ("Spiel beenden".GetHashCode(), gameRect, gameWindow, "Spiel beenden");
		serverRect		= GUI.Window ("Server eröffnen".GetHashCode(), serverRect, serverCreation, "Server eröffnen");
		rulesRect		= GUI.Window ("Anleitung".GetHashCode(), rulesRect, rules, "Anleitung");
	}
	else if (Network.peerType != NetworkPeerType.Disconnected && !menuActive) {
		if(showScoreboard) {
			scoreRect		= GUI.Window ("Scoreboard".GetHashCode(), scoreRect, score, "Scoreboard");
		}
	}
}

function OnFailedToConnect(error: NetworkConnectionError) {
	switch(error.ToString()) {
		case "RSAPublicKeyMismatch":			statMsg = "Öffentliche RSA-Keys stimmen nicht überein:\nVeraltete Version?..."; break;
		case "InvalidPassword":					statMsg = "Ungüges Passwort:\nEingaben überprüfen!"; break;
		case "ConnectionFailed":				statMsg = "Verbindung fehlgeschlagen:\nNetzwerk-Verbindungen überprüfen!"; break;
		case "TooManyConnectedPlayers":			statMsg = "Spielerlimit des Servers erreicht:\nEine Meldung mit der ich nicht gerechnet hätte."; break;
		case "ConnectionBanned":				statMsg = "Du wurdest gebannt:\nProbiere es später noch einmal, oder wechsle den Server!"; break;
		case "AlreadyConnectedToServer":		statMsg = "Bereits verbunden:\nDu befindest dich bereits auf diesem Server!"; break;
		case "AlreadyConnectedToAnotherServer":	statMsg = "Noch verbunden:\nEigentlich unmöglich, aber verlasse zuerst den Server!"; break;
		case "CreateSocketOrThreadFailure":		statMsg = "Fehler beim Erstellen des Netzwekinterfaces:\nPort vielleicht bereits in Nutzung?"; break;
		case "IncorrectParameters":				statMsg = "Ungültige Parameter:\nEingaben überprüfen!"; break;
		case "EmptyConnectTarget":				statMsg = "Kein Host angegeben:\nIP oder Hostername eingeben."; break;
		case "InternalDirectConnectFailed":		statMsg = "Netzwerkinterner-Fehler:\nKeine direkte Verbindung durch NAT möglich."; break;
		case "NATTargetNotConnected":			statMsg = "Ziel nicht mit Vermittlungsserver verbunden:\nDa kannst du direkt leider gar nichts dran ändern. :("; break;
		case "NATTargetConnectionLost":			statMsg = "NAT-Verbindung während Verbindungsaufbau verloren:\nÜberprüfe deine Router-Einstellungen."; break;
		case "NATPunchthroughFailed":			statMsg = "NAT-Punchthrough gescheitert:\nDie NAT-Einstellungen zu streng, oder kein Server läuft an dieser IP."; break;
	}
	StopCoroutine("rotStat");
	rotTime		= 0;
	j			= 0;
}

// Makes every client change the level to lvlID
function globalLoadLevel(lvlID : String) {
	Network.RemoveRPCsInGroup(0);
	Network.RemoveRPCsInGroup(1);
	networkView.RPC("loadLevel", RPCMode.AllBuffered, lvlID, lastLevelPrefix + 1);	
}

// Abandons network trafic and loads a new level.
@RPC
function loadLevel (level : String, levelPrefix : int) {
	lastLevelPrefix = levelPrefix;

		// There is no reason to send any more data over the network on the default channel,
		// because we are about to load the level, thus all those objects will get deleted anyway
		Network.SetSendingEnabled(0, false);	

		// We need to stop receiving because first the level must be loaded first.
		// Once the level is loaded, rpc's and other state update attached to objects in the level are allowed to fire
		Network.isMessageQueueRunning = false;

		// All network views loaded from a level will get a prefix into their NetworkViewID.
		// This will prevent old updates from clients leaking into a newly created scene.
		Network.SetLevelPrefix(levelPrefix);
		Application.LoadLevel(level);
		yield;
		yield;

		// Allow receiving data again
		Network.isMessageQueueRunning = true;
		// Now the level has been loaded and we can start sending out data to clients
		Network.SetSendingEnabled(0, true);

		for (var go in FindObjectsOfType(GameObject))
			go.SendMessage("OnNetworkLoadedLevel", SendMessageOptions.DontRequireReceiver);	
}