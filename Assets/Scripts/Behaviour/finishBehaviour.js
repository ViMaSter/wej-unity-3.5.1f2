#pragma strict
#pragma implicit
#pragma downcast

//settings

// relations - Objects

// relations - Get Objects on Startup

function Start () {

}

function OnCollisionEnter(col : Collision) {
	if (col.transform.name == "Player" && col.transform.parent.networkView.isMine && Application.loadedLevelName.Split("_"[0])[1] == "time") {
		var s : scores = GameObject.Find("Spawn").GetComponent("scores") as scores;
		s.endGame(true);
	}
}