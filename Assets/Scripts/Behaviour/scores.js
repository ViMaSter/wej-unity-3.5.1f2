#pragma strict
#pragma implicit
#pragma downcast

//settings
	var myPlayer		: String;
	var	curScore		: int;
	var otherScores		: Hashtable;
	var otherTimes		: Hashtable;
	var hashNames		: String[];
	var hashScores		: String[];
	var hashTimes		: String[];
	var timerRuns		: boolean;
	
	var respawnAt		: float;
	var respawnTime		: float;
	
	var lastTimer		: boolean		= true;
	var finishTimer		: float			= 0;
	var finishTolerance	: int			= 15;

// relations - Objects

// relations - Get Objects on Startup
function Awake () {
	if (Network.isServer) {
		otherScores		=	new Hashtable();
		otherTimes		=	new Hashtable();
	}
	else {
		otherScores		=	new Hashtable();
		otherTimes		=	new Hashtable();
		getScores();
	}
}

// Initiates respawning
function respawn(time : int) {
	// relations
	var i : Transform			= (GetComponent("Instantiate") as Instantiate).yourPlayer.Find("Player");
	var m : movement			= i.GetComponent("movement") as movement;
	
	// deactivate controls and collision
	i.collider.isTrigger			= true;
	i.rigidbody.useGravity			= false;
	i.rigidbody.velocity			= Vector3.zero;
	m.controlStuff					= false;
	
	// calculate time until respawn
	respawnAt						= Time.time + time;
	respawnTime						= respawnAt - Time.time;

	// wait until time is over
	while (respawnTime > 0) {
		respawnTime		= respawnAt - Time.time;
		yield;
	}
	
	// reactiavte and reset everything
	i.collider.isTrigger			= false;
	m.controlStuff					= true;
	i.rigidbody.useGravity			= true;
	i.rigidbody.velocity			= Vector3.zero;
	i.transform.position			= Vector3.zero;
	respawnTime		= 0;
}

// Saves the scores locally
@RPC
function setScores (nameString : String, scoreString : String) {
	if (!Network.isServer) {
		var names : String[] = nameString.Split(","[0]);
		var scores : String[] = scoreString.Split(","[0]);
		
		for (var i = 0; i < names.Length; i++) {
			try {
				otherScores[names[i]]	= scores[i];
			} catch (e) {
				otherScores.Add(names[i], scores[i]);
			}
		}
	}
}

// Saves the times locally
@RPC
function setTimes (nameString : String, timeString : String) {
	if (!Network.isServer) {
		var names		: String[]		= nameString.Split(","[0]);
		var times		: String[]		= timeString.Split(","[0]);
		var setTime		: boolean		= false;
		
		for (var i = 0; i < names.Length; i++) {
			try {
				otherTimes[names[i]]	= times[i];
			} catch (e) {
				otherTimes.Add(names[i], "0");
			}
			Debug.Log("Checkdis: " + times[i]);
			if (times[i] != "0") {
				setTime = true;
			}
		}
		if (setTime && finishTimer == 0) {
			var g : gamemode	= GetComponent("gamemode") as gamemode;
			var t : float 		= g.readyTime + 5 - Time.timeSinceLevelLoad * -1;
			finishTimer = t;
			if (!lastTimer) {
				setLastTimer();	
				lastTimer = true;
			}
		}
	}
}

// Starts the remaining time-countdown before ending the game
function setLastTimer() {
		yield WaitForSeconds(finishTolerance);
		if (otherTimes[myPlayer] == "0")
			endGame(false);
}

// Makes the server send the current scores to everybody (update)
@RPC
function getScores () {
	if (!Network.isServer)
		networkView.RPC("sendScores", RPCMode.Server);
}

// Makes the server send the current times to everybody (update)
@RPC
function getTimes () {
	if (!Network.isServer)
		networkView.RPC("sendTimes", RPCMode.Server);
}

// Send the current scores to everybody
@RPC
function sendScores () {
	var tmpHashNames		: String[]	= new String[otherScores.Count];
	var tmphashScores		: String[]	= new String[otherScores.Count];
	otherScores.Keys.CopyTo(tmpHashNames, 0);
	otherScores.Values.CopyTo(tmphashScores, 0);
	var hashNames			: String	= Array(tmpHashNames).Join(",");
	var hashScores			: String	= Array(tmphashScores).Join(",");
	networkView.RPC("setScores", RPCMode.All, hashNames, hashScores);
}

// Send the current times to everybody
@RPC
function sendTimes () {
	var tmpHashNames		: String[]	= new String[otherTimes.Count];
	var tmphashTimes		: String[]	= new String[otherTimes.Count];
	otherTimes.Keys.CopyTo(tmpHashNames, 0);
	otherTimes.Values.CopyTo(tmphashTimes, 0);
	var hashNames			: String	= Array(tmpHashNames).Join(",");
	var hashTimes			: String	= Array(tmphashTimes).Join(",");
	networkView.RPC("setTimes", RPCMode.All, hashNames, hashTimes);
}

// Informs current clients about a new player
@RPC
function announcePlayer (username : String) {
	//TODO: Insert Double-Naming-Prevention here
	Debug.Log("Announce:" + username);
	addPlayer(username);
	networkView.RPC("getScores", RPCMode.Others);
	networkView.RPC("getTimes", RPCMode.Others);
}

// Adds a player to the time- and scoreboard
@RPC
function addPlayer (username : String) {
	if (otherScores[username] == null) {
		Debug.Log("Add:" + username);
		otherScores.Add(username, "0");
		otherTimes.Add(username, "0");
		getScores();
		getTimes();
	}
}

// Updates the current scores
@RPC
function score (user : String, amount : int) {
	Debug.Log("Remote: " + user + ": " + amount + "\n" + "Current: " + otherScores[user].ToString());
	otherScores[user] = (System.Int32.Parse(otherScores[user]) + amount).ToString();
	networkView.RPC("getScores", RPCMode.All);
}

// Inform 
@RPC
function finish (user : String, inFinish : boolean) {
	var g : gamemode	= GetComponent("gamemode") as gamemode;
	if (inFinish) {
		if (otherTimes[user] == "0") {
			var t : float 		= g.readyTime + 5 - Time.timeSinceLevelLoad * -1;
			Debug.Log("Remote-Finish: " + user + "\n" + "Time: " + t);
			otherTimes[user] = t.ToString();
			if (finishTimer == 0)
				finishTimer = t;
			sendTimes();
		}
	}
	else {
		if (otherTimes[user] == "0") {
			Debug.Log("Forced finish for " + user + ".\n Didn't reached finish fast enough.");
			otherTimes[user] = "DNF";
			sendTimes();
		}
		else {
			Debug.Log("Cheatz! Hackz! L33tz! ...or just bad codez.\nUser " + user + " already had a time!");
			//to run this you have to be cheating or not synchronized
			//there's a value in otherScores about this client already, but he's not in finish.
		}
	}
}


function getOwnPoints() {
	return otherScores[myPlayer];
}

function getOwnTime() {
	return otherScores[myPlayer];
}

function Update () {

}

//used to freeze/deactivate controlls for a player, is he reaches the finish or the timeout is smaller then 0
@RPC
function endGame (inFinish : boolean) {
	var i : Transform			= (GetComponent("Instantiate") as Instantiate).yourPlayer.Find("Player");
	var m : movement			= i.GetComponent("movement") as movement;
	var g : gamemode			= GetComponent("gamemode") as gamemode;
	
	i.collider.isTrigger			= true;
	i.rigidbody.useGravity			= false;
	m.controlStuff					= false;
	i.rigidbody.velocity			= Vector3.zero;
	
	var p : Vector3					= transform.position;
		p.y							= p.y + 2;
		
	i.transform.position			= p;
	
	if (g.gameMode != "free") {
		var c : connectionGUI			= GameObject.Find("network").GetComponent("connectionGUI") as connectionGUI;
		if (c != null)
			c.showScoreboard				= true;
		
		if (!Network.isServer)
			networkView.RPC("finish", RPCMode.Server, myPlayer, inFinish);
		else
			finish(myPlayer, inFinish);
	}
	else {
		respawn(3);
	}
}

function OnGUI () {
	var centerCenterAlign : GUIStyle = new GUIStyle();
	centerCenterAlign.alignment				= TextAnchor.MiddleCenter;
	centerCenterAlign.normal.textColor		= Color.white;
	if (respawnTime > 0) {
		GUI.Label(Rect(Screen.width / 2 - 100, Screen.height / 2 - 20, 200, 20), "Respawn in", centerCenterAlign);
		GUI.Label(Rect(Screen.width / 2 - 100, Screen.height / 2, 200, 20), respawnTime.ToString("F2"), centerCenterAlign);
	}
}