#pragma strict
#pragma implicit
#pragma downcast

function Start () {
	// Used to destroy coins, when not playing the score-gametype
	if (Application.loadedLevelName.Split("_"[0])[1] != "score")
		Destroy(gameObject);
}