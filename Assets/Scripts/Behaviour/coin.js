#pragma strict
#pragma implicit
#pragma downcast

//settings
	var	itemKind		: int;
	var itemValue		: int;
	var deleteAni		: Transform;

// relations - Objects

// relations - Get Objects on Startup
function Update () {
	transform.eulerAngles.y += Time.deltaTime * 20;
}

// runs when... collected
function collected() {
	renderer.enabled = false;
	collider.enabled = false;
	Destroy(transform.Find("Sphere").gameObject);
	var ro = Quaternion.identity;
	ro.eulerAngles = Vector3(270, 0, 0);
	var tmp : Transform = Instantiate(deleteAni, Vector3.zero, ro);
	tmp.parent			= transform;
	tmp.localPosition	= Vector3.zero;;
	yield WaitForSeconds(0.2);
	Destroy(transform.Find("GameObject").gameObject);
	yield WaitForSeconds(1);
	Destroy(gameObject);
}