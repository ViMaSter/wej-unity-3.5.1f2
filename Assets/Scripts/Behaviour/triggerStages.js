#pragma strict
#pragma implicit
#pragma downcast

//settings
		var stageToCall		: int			= 0;

//internal and debug variables
private	var jumped			: int			= 0;
private	var aJumpDeadz		: int			= 50;

// relations - Objects
private	var optionObj		: GameObject;

// relations - Scripts
private	var generatorScript	: generator;

// relations - Get Objects on Startup
function setRelations () {
}
function Awake () {
	setRelations();
}

// Activates other parts of the map
function OnCollisionEnter (cObj : Collision) {
	if (cObj.transform.name == "Player") {
		generatorScript	= cObj.transform.parent.Find("Options").GetComponent("generator");
		generatorScript.callLevelArea(stageToCall);
	}
}