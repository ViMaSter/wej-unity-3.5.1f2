#pragma strict
#pragma implicit
#pragma downcast

//settings
		var gameMode			: String;
		var gameState			: int;
		//	0 = Pre-Countdown
		//	1 =  3 Sec. Countdown
		//	2 = In-game
		//	3 = 10 Sec. Coundown
		//	4 = Game-ended
		var gameModeDictionary	: Hashtable;
		var yourPlayer			: Transform;
		var movementScript		: movement;
		var	readyTime			: float;
private	var	playerReady			: Array;
		var startedGame			: boolean	= false;
		var	serverAlive			: boolean	= false;
		var	timerOverNull		: boolean	= false;
		var rightAlign			: GUIStyle;
		
		var	finishTimes			: Hashtable;

// relations - Objects

// relations - Get Objects on Startup
function setRelations () {
	gameMode					= Application.loadedLevelName.Split("_"[0])[1];
	gameModeDictionary			= new Hashtable();
	finishTimes					= new Hashtable();
	playerReady					= new Array();
	gameModeDictionary.Add("free",		"Freeroaming"); 
	gameModeDictionary.Add("time",		"Zeitrennen");
	gameModeDictionary.Add("score",		"Punktejagd");
}

// Destorys the players object and switches the camera
// Used when joining a game after it started
@RPC
function killYourself () {
	Debug.Log("Kill!");
	var o : GameObject	= GameObject.Find("playerObj(Clone)");
	var u : Username	= o.Find("Username").GetComponent("Username") as Username;
	u.watch();
	o.Find("MainCamera").camera.enabled = true;
}

// Used when joining a game before it starts
@RPC
function authorised () {
	Debug.Log("Go!");
	var i : Instantiate			= GetComponent("Instantiate") as Instantiate;
	i.instantiatePlayer();
	yield;
	while (movementScript == null) {yield;}
	var u : Username			= yourPlayer.Find("Username").GetComponent("Username") as Username;
	if (Network.isServer)		signReady(u.Username);
	else						networkView.RPC("signReady", RPCMode.Server, u.Username);
}

// Network test used to determine if the connection is ready after joining a game
@RPC
function ping(info : NetworkMessageInfo) {
	networkView.RPC("pong", info.sender);
}

@RPC
function pong() {
	serverAlive = true;
}

// Handles new users and get's called every time a user joins
@RPC
function ask (info : NetworkMessageInfo) {
	Debug.Log("got asked");
	if (gameMode == "free" || (gameMode != "free" && startedGame == false)) {
		// Allow joining
		Debug.Log("answer: yes");
		networkView.RPC("authorised", info.sender);
	}
	else {
		// Disallow joining
		Debug.Log(startedGame.ToString() + " | Mode: " + gameMode);
		Debug.Log("answer: no");
		networkView.RPC("killYourself", info.sender);
	}
}

// Ran right after joining a network game
function checkForSpawn () {
	// As long as the server isn't "alive"...
	while (!serverAlive) {
		Debug.Log("Waiting for Server response...");
		//...ask if he is...
		networkView.RPC("ping", RPCMode.Server);
		//...wait for the answer
		yield;
		//...and repeat if necessary
	}
	// When server is alive ask if the client can still join the game in progress
	networkView.RPC("ask", RPCMode.Server);
	Debug.Log("ask");
}

function Awake () {
	setRelations();
	if (Network.isClient) {
		checkForSpawn();
	}
	else {
		Debug.Log("authAsServer");
		pong();
		authorised();
	}
}

// Used to determine which object is yours when instantiating is done
function setPlayerObj (player : Transform) {
	yourPlayer					= player;
	movementScript				= yourPlayer.Find("Player").GetComponent("movement");
}

// Used when a client is "ready"
@RPC
function signReady (userName : String, info : NetworkMessageInfo) {
	if (gameMode == "free" || (gameMode != "free" && startedGame == false)) {
		Debug.Log(userName + " is ready,");
		playerReady.Add(userName);
	}
	else {
		networkView.RPC("killYourself", info.sender);
	}
}

// Used when being a server - same as above
function signReady (userName : String) {
	Debug.Log(userName + "(Server) is ready,");
	playerReady.Add(userName);
}

// Starts the game(-countdown)
@RPC
function startGame () {
	Debug.Log("Start the game!\n"+Time.timeSinceLevelLoad);
	readyTime = Time.timeSinceLevelLoad;
	startedGame = true;
	gameState	= 1;
	yield WaitForSeconds(3);
	gameState	= 2;
}

function Update () {
	// Starts game if client is serving and every player is "ready"
	if (Network.isServer && playerReady.length == (Network.connections.Length + 1) && !startedGame) {
		Debug.Log("EVERYBODY is ready, " + Network.connections.Length);
		networkView.RPC("startGame", RPCMode.All);
	}
	
	// Ready the controls as soon as the game starts
	if (gameMode == "free" && movementScript != null)
		movementScript.enabled = true;
	else {
		if (startedGame && Time.timeSinceLevelLoad - readyTime > 5)
			movementScript.enabled = true;
	}
}

// Formats the timer string
function formatTimer (curTime : float) {
	var	intTime 	: int		= curTime;
	var	minutes		: int		= intTime / 60;
	var	seconds		: int		= intTime % 60;
	var	fraction	: int		= curTime * 1000;
		fraction				= fraction % 1000;
	var	timeText	: String	= String.Format ("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction);
	return timeText;
}

function OnGUI () {
	var timer : float = readyTime + 5 - Time.timeSinceLevelLoad;
	if (gameMode != "free") {
		if (startedGame && Mathf.Ceil(timer) > 0) {
			GUI.Label(Rect(Screen.width / 2 - 200, Screen.height / 2 - 40, 200, 20), "Spielstart in...");
			GUI.Label(Rect(Screen.width / 2 - 40, Screen.height / 2 - 20, 40, 20), Mathf.Ceil(timer).ToString());
		}
		else if (startedGame && Mathf.Ceil(timer) <= 0 && Mathf.Ceil(timer) >= -1) {
			GUI.Label(Rect(Screen.width / 2 - 40, Screen.height / 2 - 20, 40, 20), "GO!");
		}
		if (timer < 0) {
			var g : gamemode	= GetComponent("gamemode") as gamemode;
			var t : float 		= g.readyTime + 5 - Time.timeSinceLevelLoad * -1;
			var s : scores		= GetComponent("scores") as scores;
			switch (gameMode) {
				case "free":
					break;
				case "time":
					if (s.finishTimer != 0) {
						GUI.Label(Rect (Screen.width - 200, 0, 200, 40), "Verbleibende Zeit:" + formatTimer((s.finishTimer + 10) - t), rightAlign);
						if (s.otherTimes[s.myPlayer] != "0")
							GUI.Label(Rect(Screen.width - 200, 20, 200, 20), "Deine Zeit: " + formatTimer(s.otherTimes[s.myPlayer]), rightAlign);
					}
					else
						GUI.Label(Rect(Screen.width - 200, 0, 200, 20), "Aktuelle Zeit: " + formatTimer(timer * -1), rightAlign);
					break;
				case "score":					
					GUI.Label(Rect(Screen.width - 200, 0, 200, 20), "Verbleibende Zeit: " + formatTimer(timer * -1), rightAlign);
					GUI.Label(Rect(Screen.width - 200, 20, 200, 20), "Punkte: " + s.getOwnPoints(), rightAlign);
					break;
			}
		}
	}
}