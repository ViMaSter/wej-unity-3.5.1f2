#pragma strict
#pragma implicit
#pragma downcast

//settings

// relations - Objects

// relations - Get Objects on Startup

function OnTriggerEnter(col : Collider) {
	if (col.transform.tag == "item" && networkView.isMine) {
		var details : coin	= col.GetComponent("coin");
		// possible switch for different items
		switch (details.itemKind) {
			case 0:
				var tmp : Username = transform.parent.Find("Username").GetComponent("Username") as Username;
				// Count local if host, send "count" to server if client
				if (Network.isServer) {
					var sc : scores	= GameObject.Find("Spawn").GetComponent("scores") as scores;
					sc.score(tmp.Username, details.itemValue);
				}
				else
					GameObject.Find("Spawn").networkView.RPC("score", RPCMode.Server, tmp.Username, details.itemValue);
				
				break;
		}
		// destroy the item
		var cScript : coin = col.gameObject.GetComponent("coin") as coin;
		cScript.collected();
	}
}