#pragma strict
#pragma implicit
#pragma downcast

//settings
	var	levelArea		: Vector3[];
	var unlockedAreas	: boolean[];

// relations - Objects
		var	levelAdditions	: GameObject[];
		var spawnObjs		: int;

// relations - Get Objects on Startup
function Start () {
	setSpawnPositions();
}

// Get and store every "unlockable" level element
function setSpawnPositions () {
	spawnObjs			= GameObject.FindGameObjectsWithTag("levelSpawn").Length;
	levelArea			= new Vector3	[parseInt(spawnObjs)];
	levelAdditions		= new GameObject[parseInt(spawnObjs)];
	for (i = 0; i < spawnObjs; i++) {
		levelArea[i]	= GameObject.Find("level" + (i+2) + "_spawn").transform.position;
	}
}

// Unlocks every area
function callAllLevelArea() {
	for (i = 0; i < spawnObjs; i++) {
		var getResource						: GameObject = Resources.Load("stages/1/stage" + (i + 2));
		if (levelAdditions[i] == null && !GameObject.Find("stage" + (i + 2))) {
			levelAdditions[i]		= Instantiate(getResource, levelArea[i], Quaternion.identity);
			levelAdditions[i].name	= getResource.name;
		}
	}
}

// Unlock a certain area
function callLevelArea (ChoosenNumber : int) {
	try {
		var getResource						: GameObject = Resources.Load("stages/1/stage" + (ChoosenNumber + 2));
		if (levelAdditions[ChoosenNumber] == null && !GameObject.Find("stage" + (ChoosenNumber + 2))) {
			levelAdditions[ChoosenNumber]		= Instantiate(getResource, levelArea[ChoosenNumber], Quaternion.identity);
			levelAdditions[ChoosenNumber].name	= getResource.name;
		}
		else if (levelAdditions[ChoosenNumber] != null) {
			print ("Already Created."	+ "(Tried " + "stages/1/stage" + (ChoosenNumber + 2) + ")");
		}
		else if (getResource == null) {
			print ("Not avalible."		+ "(Tried " + "stages/1/stage" + (ChoosenNumber + 2) + ")");
		}
	}
	catch (e) {
		print ("Other error: \n" + e.ToString());
	}
}