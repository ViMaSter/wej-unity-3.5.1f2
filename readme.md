Working demos:  
[win_x86](http://squaretimes.de/dev/vi/ga/alpha_0.8/win_x86.zip) - 
[win_x64](http://squaretimes.de/dev/vi/ga/alpha_0.8/win_x64.zip) - 
[osx](http://squaretimes.de/dev/vi/ga/alpha_0.8/osx.zip) - 
[web](http://squaretimes.de/dev/vi/ga/alpha_0.8/web/)
  
  
__Working:__  
Main Menu:  

*   Avalible as In-Game Menu via "Esc"-Key or "Start"-Button
*   Only need to hide the "Quit Game"-Button in the Web-version

Multiplayer:  

*   Serverbrowser
*   Servercreation
*   Joining games
*   Voting

In-Game:  

*   (Xbox 360-)Controller Support

Missing:  

*   Time-Attack: ___Finishing___ only works local. Except for the scoreboard, which blocks the view of every player.
*   Score-Hunt: Finishing __works__ as in Time-Attack - so... it doesn't.